# Docker publish

This job configuration allows you to push docker-image to container registry.

### How to include
```yml
stages:
  - publish

include:
  - project: 'aanaz/devops-includes/gitlab-ci-extends/includes/docker-publish'
    ref: main
    file: docker-publish.yml

docker-publish:
  extends: .docker-publish
  variables:
    DOCKER_PUBLISH_IMAGE_NAME:
```

### Variables

| VARIABLE | BY DEFAULT | DESCRIPTION |
| --- | :---: | --- |
`DOCKER_PUBLISH_IMAGE_NAME` | `${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}` | Name of image
